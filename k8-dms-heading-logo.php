<?php
/*
	Plugin Name: K8 Heading Logo DMS Section
	Plugin URI: http://www.kri8it.com
	Description: Add a heading logo to your site
	Author: Charl Pretorius
	PageLines: true
	Version: 1.0.1
	Section: true
	Class Name: K8HeadingLogo
	Filter: component
	Loading: active
	
	K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-dms-heading-logo
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;

	
class K8HeadingLogo extends PageLinesSection {
	
	function section_persistent(){
        
		
		
    }
	
	function before_section_template( $location = '' ) {

		$this->wrapper_classes['pad-set'] = ($this->opt('no_pad')) ? 'no-pad' : '';
	
	}
			
	function section_template() {
				
		echo ( is_home() || is_front_page() ? '<h1>' : '<h2>' );	
			echo '<a class="site-logo" title="' . get_bloginfo('name') . '" href="' . home_url('/') . '">' . $this->image( 'k8_heading_logo', pl_get_theme_logo(), array(), get_bloginfo('name')) . '</a>';		
		echo ( is_home() || is_front_page() ? '</h1>' : '</h2>' );
		
	}

	function section_opts(){
						
		$opts = array(
			array(
				'type'		=> 'multi',
				'key'		=> 'k8_heading_logo_settings',
				'col'		=> 1,
				'opts'		=> array(					
					array(
			            'key'           => 'k8_heading_logo',
			            'type'          => 'image_upload',
			            'label'			=> __( 'Site Logo', 'pagelines' ),
			            'has_alt'		=> true,
			            'imgsize'       => '64',        // The image preview 'max' size
			            'sizelimit'     => '2000000'     // Image upload max size default 512kb
			        ),
			        array(
						'type'			=> 'check',
						'key'			=> 'no_pad', 
						'label'			=> __( 'Remove Media Box padding (Set to 0px)?', 'pagelines' )
					),						
				)
			)
		);
		return $opts;		
	}
}